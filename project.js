var net = require('net');

var server = net.createServer(function(connection){
    console.log('connected');

    connection.on('data', function(data){
        console.log(data + 'from' + connection.remoteAddress + '' + connection.remotePort);
        connection.write('Repeat: ' + data);
    });

    connection.on('close', function(){
        console.log('connection lost');
    });
}).listen(8124);
console.log('listen 8124');
